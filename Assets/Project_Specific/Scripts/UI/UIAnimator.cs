using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SakuraGamesSDK;
using DG.Tweening;

public class UIAnimator : MonoBehaviour
{
    [SerializeField] protected float m_Duration = 0.25f;
    [SerializeField] private Image m_Background;
    [SerializeField] private float m_DefaultBGOpacity = 0.35f;

    protected virtual void OnEnable()
    {
        if (!m_Background) return;

        m_Background.DOFade(0, 0);
        m_Background.DOFade(m_DefaultBGOpacity, m_Duration);
    }

    public virtual void Hide()
    {
        DOVirtual.DelayedCall(m_Duration, () => gameObject.SetActive(false), false);

        if (!m_Background) return;

        m_Background.DOFade(m_DefaultBGOpacity, 0);
        m_Background.DOFade(0, m_Duration);
    }
}
