using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SakuraGamesSDK;


public class ProgressBar : Singleton<ProgressBar>
{
    [SerializeField] private Image m_Fill;
    private float m_Amount;

    #region Init 

    private void OnEnable()
    {
        GameManager.OnLevelLoaded += OnLevelLoaded;
    }

    private void OnDisable()
    {
        GameManager.OnLevelLoaded -= OnLevelLoaded;
    }

    #endregion

    #region Callbacks

    private void OnLevelLoaded()
    {
        m_Amount = 0;
        m_Fill.fillAmount = 0;
    }

    #endregion

    #region UnityLoop

    private void Update()
    {
        float progress = (float)PlayerController.Instance.EatenBotsCount / (float)(GameConfig.Instance.CountToSizeUp
            * (PlayerController.Instance.Size + 1));

        if (m_Amount < progress)
            m_Amount += 0.01f;
        if (m_Amount > progress)
            m_Amount -= 0.01f;

        m_Fill.fillAmount = m_Amount;
    }

    #endregion
}
