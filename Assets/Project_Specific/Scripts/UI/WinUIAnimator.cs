using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WinUIAnimator : UIAnimator
{
    [SerializeField] private RectTransform m_WinText;
    [SerializeField] private RectTransform m_Chest;
    [SerializeField] private RectTransform m_PlayButton;
    [SerializeField] private Ease m_EaseType;
    [SerializeField] private float m_StartPlayButtonY;
    [SerializeField] private float m_EndPlayButtonY;

    protected override void OnEnable()
    {
        base.OnEnable();

        m_WinText.transform.DOScale(0, 0);
        m_WinText.transform.DOScale(1, m_Duration).SetEase(m_EaseType);

        m_Chest.transform.DOScale(0, 0);
        m_Chest.transform.DOScale(1, m_Duration).SetEase(m_EaseType);

        m_PlayButton.DOAnchorPosY(m_StartPlayButtonY, 0);
        m_PlayButton.DOAnchorPosY(m_EndPlayButtonY, m_Duration).SetEase(m_EaseType);
    }

    public override void Hide()
    {
        base.Hide();

        m_WinText.transform.DOScale(1, 0);
        m_WinText.transform.DOScale(0, m_Duration).SetEase(m_EaseType);

        m_Chest.transform.DOScale(1, 0);
        m_Chest.transform.DOScale(0, m_Duration).SetEase(m_EaseType);

        m_PlayButton.DOAnchorPosY(m_EndPlayButtonY, 0);
        m_PlayButton.DOAnchorPosY(m_StartPlayButtonY, m_Duration).SetEase(m_EaseType);
    }
}
