using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class InGameUIAnimator : UIAnimator
{
    [SerializeField] private RectTransform m_Container;
    [SerializeField] private float m_StartY;
    [SerializeField] private float m_EndY;
    [SerializeField] private Ease m_EaseType;

    protected override void OnEnable()
    {
        base.OnEnable();

        m_Container.DOAnchorPosY(m_StartY, 0);
        m_Container.DOAnchorPosY(m_EndY, m_Duration).SetEase(m_EaseType);
    }

    public override void Hide()
    {
        base.Hide();

        m_Container.DOAnchorPosY(m_EndY, 0);
        m_Container.DOAnchorPosY(m_StartY, m_Duration).SetEase(m_EaseType);
    }
}
