using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;
using TMPro;
using DG.Tweening;

public class CoinCounterText : Singleton<CoinCounterText>
{
    [SerializeField] public Transform CoinIcon;
    [SerializeField] private TextMeshProUGUI m_Text;
    private int m_Count;

    #region Init

    private void OnEnable()
    {
        GameManager.OnLevelLoaded += OnLevelLoaded;
        OnLevelLoaded();
    }

    private void OnDisable()
    {
        GameManager.OnLevelLoaded -= OnLevelLoaded;
    }

    #endregion

    #region Callbacks

    private void OnLevelLoaded()
    {
        m_Count = StorageManager.Instance.Coins;
    }

    #endregion

    #region Unity Loop

    private void Update()
    {
        m_Text.text = m_Count.ToString();
        if (m_Count < StorageManager.Instance.Coins)
            m_Count++;
    }

    #endregion

    #region Public

    public void PunchCoin()
    {
        CoinIcon.DOKill();
        CoinIcon.DOScale(1.2f, 0.2f).OnComplete(() => CoinIcon.DOScale(1, 0.2f));
    }

    #endregion
}
