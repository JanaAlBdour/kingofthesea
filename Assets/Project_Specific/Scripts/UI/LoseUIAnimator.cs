using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LoseUIAnimator : UIAnimator
{
    [SerializeField] private RectTransform m_LoseText;
    [SerializeField] private RectTransform m_ReasonText;
    [SerializeField] private RectTransform m_Skull;
    [SerializeField] private RectTransform m_NextButton;
    [SerializeField] private Ease m_EaseType;
    [SerializeField] private float m_StartPlayButtonY;
    [SerializeField] private float m_EndPlayButtonY;

    protected override void OnEnable()
    {
        base.OnEnable();

        m_LoseText.transform.DOScale(0, 0);
        m_LoseText.transform.DOScale(1, m_Duration).SetEase(m_EaseType);

        m_ReasonText.transform.DOScale(0, 0);
        m_ReasonText.transform.DOScale(1, m_Duration).SetEase(m_EaseType);

        m_Skull.transform.DOScale(0, 0);
        m_Skull.transform.DOScale(1, m_Duration).SetEase(m_EaseType);

        m_NextButton.DOAnchorPosY(m_StartPlayButtonY, 0);
        m_NextButton.DOAnchorPosY(m_EndPlayButtonY, m_Duration).SetEase(m_EaseType);
    }

    public override void Hide()
    {
        base.Hide();

        m_LoseText.transform.DOScale(1, 0);
        m_LoseText.transform.DOScale(0, m_Duration).SetEase(m_EaseType);

        m_ReasonText.transform.DOScale(1, 0);
        m_ReasonText.transform.DOScale(0, m_Duration).SetEase(m_EaseType);

        m_Skull.transform.DOScale(1, 0);
        m_Skull.transform.DOScale(0, m_Duration).SetEase(m_EaseType);

        m_NextButton.DOAnchorPosY(m_EndPlayButtonY, 0);
        m_NextButton.DOAnchorPosY(m_StartPlayButtonY, m_Duration).SetEase(m_EaseType);
    }
}
