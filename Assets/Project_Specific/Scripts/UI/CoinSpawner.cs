using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;
using DG.Tweening;

public class CoinSpawner : Singleton<CoinSpawner>
{
    [SerializeField] private Transform m_Canvas;
    [SerializeField] private GameObject m_CoinPrefab;
    [SerializeField] private int m_MaxCoins = 25;
    [SerializeField] private int m_MinCoins = 1;
    [SerializeField] private float m_Duration = 0.5f;
    [SerializeField] private float m_Delay = 0.05f;

    #region Init

    private void OnEnable()
    {
        StorageManager.OnGainCoins += OnGainCoins;
    }

    private void OnDisable()
    {
        StorageManager.OnGainCoins -= OnGainCoins;
    }

    #endregion

    #region Callbacks

    private void OnGainCoins(int i_Coins, Vector3 i_Position)
    {
        int gain = Mathf.Clamp(i_Coins / 3, m_MinCoins, m_MaxCoins);
        StopAllCoroutines();
        StartCoroutine(CreateCoins(gain, i_Position));
    }

    IEnumerator CreateCoins(int i_Gain, Vector3 i_Position)
    {
        for (int i = 0; i < i_Gain; i++)
        {
            GameObject coin = Instantiate(m_CoinPrefab, i_Position,
                Quaternion.identity, m_Canvas);
            coin.transform.DOMove(CoinCounterText.Instance.CoinIcon.position, m_Duration)
                .OnComplete(() =>
                {
                    CoinCounterText.Instance.PunchCoin();
                    Destroy(coin);
                });

            yield return new WaitForSeconds(m_Delay);
        }
    }

    #endregion
}
