using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MoreMountains.NiceVibrations;

public class HomeUIAnimator : UIAnimator
{
    [SerializeField] private RectTransform m_VibrationButton;
    [SerializeField] private RectTransform m_GameName;
    [SerializeField] private float m_StartNameX;
    [SerializeField] private float m_EndNameX;
    [SerializeField] private RectTransform m_PlayButton;
    [SerializeField] private float m_StartButtonY;
    [SerializeField] private float m_EndButtonY;
    [SerializeField] private Ease m_EaseType;

    protected override void OnEnable()
    {
        m_VibrationButton.gameObject.SetActive(MMVibrationManager.HapticsSupported());

        m_VibrationButton.transform.DOScale(0, 0);
        m_VibrationButton.transform.DOScale(1, m_Duration).SetEase(m_EaseType);

        m_PlayButton.DOAnchorPosY(m_StartButtonY, 0);
        m_PlayButton.DOAnchorPosY(m_EndButtonY, m_Duration).SetEase(m_EaseType);

        m_GameName.DOAnchorPosX(m_StartNameX, 0);
        m_GameName.DOAnchorPosX(m_EndNameX, m_Duration).SetEase(m_EaseType);

        base.OnEnable();
    }

    public override void Hide()
    {
        m_VibrationButton.transform.DOScale(1, 0);
        m_VibrationButton.transform.DOScale(0, m_Duration).SetEase(m_EaseType);

        m_PlayButton.DOAnchorPosY(m_EndButtonY, 0);
        m_PlayButton.DOAnchorPosY(m_StartButtonY, m_Duration).SetEase(m_EaseType);

        m_GameName.DOAnchorPosX(m_EndNameX, 0);
        m_GameName.DOAnchorPosX(m_StartNameX, m_Duration).SetEase(m_EaseType);

        base.Hide();
    }
}
