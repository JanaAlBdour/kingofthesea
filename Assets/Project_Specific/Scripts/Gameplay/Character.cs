using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;
using DG.Tweening;

public class Character : MonoBehaviour
{
    [HideInInspector] public int EatenBotsCount;
    [HideInInspector] public int Size;
    [SerializeField] protected CharacterController m_Controller;
    [SerializeField] protected GameObject[] m_Fish;

    protected bool Edible(Bot i_Bot, float i_Distance, float i_MaxDistance)
    {
        return i_Bot && i_Bot.Size <= Size && i_Distance <= i_MaxDistance
            && !m_ReachedLast && i_Bot != m_LastBot;
    }

    protected Bot m_LastBot;
    protected bool m_ReachedLast
    {
        get { return Size == m_Fish.Length - 1; }
    }

    public virtual void SetSize(int i_Size)
    {
        Size = i_Size; 
        for (int i = 0; i < m_Fish.Length; i++)
            m_Fish[i].SetActive(i == Size);
    }

    public int RandomSize() 
    {
        float lerped = Mathf.Lerp
            (
            GameConfig.Instance.SizeMin,
            GameConfig.Instance.SizeMax,
            (float)StorageManager.Instance.Level / (float)GameConfig.Instance.SizeLimiter
            );
        return Random.Range(Mathf.FloorToInt(lerped), Mathf.RoundToInt(lerped)); 
    }

    protected void Eat(Bot i_Bot)
    {
        m_LastBot = i_Bot;
        i_Bot.Target = transform;

        i_Bot.transform.DOScale(0, 0.5f)
            .OnComplete(() => 
            {
                BotGenerator.Instance.RecycleBot(i_Bot);
            });

        EatenBotsCount++;
        if (EatenBotsCount >= GameConfig.Instance.CountToSizeUp * (Size + 1))
        {
            EatenBotsCount = 0;
            SetSize(Size + 1);
        }

        float scale = Mathf.Lerp(1, GameConfig.Instance.MaxScale, (float)Size / (float)m_Fish.Length);
        transform.DOScale(1.5f * scale, 0.2f).OnComplete(() => transform.DOScale(scale, 0.2f));
    }
}
