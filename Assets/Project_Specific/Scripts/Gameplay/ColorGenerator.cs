using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;

public class ColorGenerator : MonoBehaviour
{
    [SerializeField] private SkinnedMeshRenderer m_Skinned;
    [SerializeField] private Mesh[] m_Meshes;

    #region Init

    private void OnEnable()
    {
        OnLevelLoaded();
        GameManager.OnLevelLoaded += OnLevelLoaded;
    }

    private void OnDisable()
    {
        GameManager.OnLevelLoaded -= OnLevelLoaded;
    }

    #endregion

    #region Callbacks

    private void OnLevelLoaded()
    {
        m_Skinned.sharedMesh = m_Meshes[Random.Range(0, m_Meshes.Length)];
    }

    #endregion
}
