using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SakuraGamesSDK;

public class BackgroundGenerator : Singleton<BackgroundGenerator>
{
    public List<Transform> Backgrounds = new List<Transform>();
    [SerializeField] private List<Vector3> m_Positions = new List<Vector3>();
    [SerializeField] private GameObject[] m_BackgroundPrefabs;

    [SerializeField] private int m_Count = 10;
    [SerializeField] private int m_Distance = 2;
    

#if UNITY_EDITOR
    public void Clear()
    {
        foreach (var bg in Backgrounds)
            DestroyImmediate(bg.gameObject);

        Backgrounds.Clear();
        m_Positions.Clear();
    }

    public void Spawn()
    {
        Clear();
        for (int x = 0; x < m_Count; x++)
        {
            for (int z = 0; z < m_Count; z++)
            {
                GameObject background = PrefabUtility.InstantiatePrefab
                    (m_BackgroundPrefabs[Random.Range(0, m_BackgroundPrefabs.Length)]) as GameObject;

                Backgrounds.Add(background.transform);

                Vector3 position = new Vector3(x * m_Distance, 0, z * m_Distance)
                    + transform.position;
                m_Positions.Add(position);

                background.transform.parent = transform;
                background.transform.position = position;
            }
        }
    }
#endif

    #region Init

    private void OnEnable()
    {
        GameManager.OnLevelLoaded += OnLevelLoaded;
    }

    private void OnDisable()
    {
        GameManager.OnLevelLoaded -= OnLevelLoaded;
    }

    #endregion

    #region Callbacks

    private void OnLevelLoaded()
    {
        List<Vector3> positions = m_Positions.GetRange(0, m_Positions.Count); ;
        for (int i = 0; i < Backgrounds.Count; i++)
        {
            int index = Random.Range(0, positions.Count);
            Backgrounds[i].transform.position = positions[index];
            positions.RemoveAt(index);

            int rot = (int)Random.Range(0, 4) * 90;
            Backgrounds[i].transform.rotation = Quaternion.Euler(0, rot, 0);
        }
    }

    #endregion
}


#if UNITY_EDITOR
[CustomEditor(typeof(BackgroundGenerator))]
class DecalMeshHelperEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        BackgroundGenerator bg = target as BackgroundGenerator;
        if (GUILayout.Button("Spawn"))
            bg.Spawn();
        if (GUILayout.Button("Clear"))
            bg.Clear();
    }
}
#endif