﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using SakuraGamesSDK;
using TMPro;

public class PlayerController : Character
{
    public static PlayerController Instance;
    [HideInInspector] public int TotalEatenFish;

    [SerializeField] private VariableJoystick m_Joystick;
    [SerializeField] private ParticleSystem m_EatEffect;
    [SerializeField] private TextMeshProUGUI m_CounterText;
    [SerializeField] private Animator[] m_Animators;

    private Vector3 m_StartMousePosition;
    private bool m_IsDragging;

    #region Init

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        GameManager.OnLevelLoaded += OnLevelLoaded;
        GameManager.OnLevelFailed += OnLevelFailed;
    }

    private void OnDisable()
    {
        GameManager.OnLevelLoaded -= OnLevelLoaded;
        GameManager.OnLevelFailed -= OnLevelFailed;
    }

    #endregion

    #region Callbacks

    private void OnLevelLoaded()
    {
        transform.position = Vector3.zero;
        transform.localScale = Vector3.one;
        EatenBotsCount = 0;
        SetSize(0);
    }

    private void OnLevelFailed()
    {
        TotalEatenFish = 0;
    }

    #endregion

    #region UnityLoop

    private void Update()
    {
        for (int i = 0; i < m_Animators.Length; i++)
            m_Animators[i].enabled = m_IsDragging && GameManager.GameState == eGameState.Playing;

        if (GameManager.GameState != eGameState.Playing) return;

        CheckDrag();
        
        if (!m_IsDragging) return;
        
        Vector3 direction = Vector3.forward * m_Joystick.Vertical
            + Vector3.right * m_Joystick.Horizontal;
        
        if (direction == Vector3.zero) return;

        m_Controller.Move(direction * Time.deltaTime * GameConfig.Instance.MovementSpeed);

        Quaternion lookRotation = Quaternion.LookRotation(direction);
        lookRotation.z = 0;
        lookRotation.x = 0;

        transform.rotation = Quaternion.Slerp
            (
            transform.rotation,
            lookRotation,
            Time.deltaTime * GameConfig.Instance.RotationSpeed
            );

        if (m_ReachedLast)
            GameManager.Instance.Complete();
       
        if (BotGenerator.Instance.Bots.Count <= 0) return;

        Bot bot = BotGenerator.Instance.Bots.GetClosestEnemy(out float distance);
        if (Edible(bot, distance, GameConfig.Instance.DistanceToEat))
        {
            TotalEatenFish++;

            Eat(bot);

            Vector3 position = Vector3.Lerp(transform.position, bot.transform.position, 0.7f);
            m_EatEffect.transform.position = position;
            m_EatEffect.Play(true);

            CameraShaker.Instance.Shake();
            HapticsManager.Instance.Haptic(HapticTypes.LightImpact);
        }
    }

    private void CheckDrag()
    {
        if (Input.GetMouseButtonDown(0))
            m_StartMousePosition = Input.mousePosition;

        if (Input.GetMouseButton(0))
        {
            Vector3 drag = (Input.mousePosition - m_StartMousePosition).normalized;
            m_IsDragging = Mathf.Abs(drag.x) > GameConfig.Instance.MovementThreshold ||
                Mathf.Abs(drag.y) > GameConfig.Instance.MovementThreshold;
        }
        else
            m_IsDragging = false;
    }

    #endregion

    public override void SetSize(int i_Size)
    {
        base.SetSize(i_Size);
        m_CounterText.text = (i_Size + 1) + "/" + m_Fish.Length;
    }
}