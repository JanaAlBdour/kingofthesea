using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;

//TODO: Add pooling
public class BotGenerator : Singleton<BotGenerator>
{
    [HideInInspector] public List<Bot> Bots = new List<Bot>();

    [SerializeField] private AIObject m_Object;
    [SerializeField] private Transform m_Player;

    [SerializeField] private Transform[] m_Waypoints;

    #region Init
    public override void Awake()
    {
        base.Awake();
        InvokeRepeating("GenerateBot", 0.1f, GameConfig.Instance.SpawnDelay);
    }

    private void OnEnable()
    {
        GameManager.OnLevelLoaded += OnLevelLoaded;
    }

    private void OnDisable()
    {
        GameManager.OnLevelLoaded -= OnLevelLoaded;
    }

    #endregion

    #region Callbacks

    private void OnLevelLoaded()
    {
        if (Bots.Count <= 0) return;

        for (int i = 0; i < Bots.Count; i++)
        {
            RecycleBot(Bots[i]);

            if (i < GameConfig.Instance.CountToSizeUp)
                Bots[i].SetSize(0);
            else
                Bots[i].SetSize(Bots[i].RandomSize());

            if (i < GameConfig.Instance.MaxBots * GameConfig.Instance.AIPercentage)
                Bots[i].IsPlayer = true;
            else
                Bots[i].IsPlayer = false;
        }
    }

    #endregion

    public Vector3 RandomPosition(float i_Distance)
    {
        Vector3 position = Random.onUnitSphere * GameConfig.Instance.BotSpawnDistance 
            + m_Player.position;
        position.y = transform.position.y;

        return GetScreenCoordinates(position, i_Distance);
    }

    Vector3 GetScreenCoordinates(Vector3 i_Input, float i_Distance)
    {
        bool minus = Random.value > 0.5f;

        Vector3 worldPoint = Camera.main.ViewportToWorldPoint(new Vector3(minus ? 0 : 1, 0, 0));
        worldPoint.x += minus ? -i_Distance : i_Distance;
        worldPoint.y = i_Input.y;
        worldPoint.z = i_Input.z;
        return worldPoint;
    }

    Quaternion RandomRotation()
    {
        return Quaternion.Euler
        (Random.Range(-20, 20), Random.Range(0, 360), 0);
    }

    private void GenerateBot()
    {
        if(m_Object.RandomizeStats)
        {
            m_Object.SpawnRate = Random.Range(1, 20); 
            m_Object.MaxSpawnAmount = Random.Range(1, 10); 
        }
        else
        {
            GameObject group = new GameObject("Bot");
            group.transform.parent = transform;

            group.SetActive(true);

            for (int i = 0; i < Random.Range(1, m_Object.MaxSpawnAmount); i++)
            {
                if (Bots.Count >= GameConfig.Instance.MaxBots) break;

                GameObject botGO = Instantiate(m_Object.Prefab, 
                    RandomPosition(GameConfig.Instance.BotSpawnDistance),
                    RandomRotation(), group.transform);

                Bot bot = botGO.GetComponent<Bot>();
                if (Bots.Count < GameConfig.Instance.CountToSizeUp)
                    bot.SetSize(0);
                else
                    bot.SetSize(bot.RandomSize());

                if (Bots.Count < GameConfig.Instance.MaxBots * GameConfig.Instance.AIPercentage)
                    bot.IsPlayer = true;
                else
                    bot.IsPlayer = false;

                Bots.Add(bot);
            }
        }
    }

    public Vector3 GetWaypoint()
    {
        int index = Random.Range(0, m_Waypoints.Length);
        return m_Waypoints[index].position;
    }

    public void RecycleBot(Bot i_Bot)
    {
        i_Bot.transform.position = RandomPosition(GameConfig.Instance.BotSpawnDistance);
        i_Bot.transform.rotation = RandomRotation();
        i_Bot.SetUpBot();
    }
}

[System.Serializable]
public class AIObject
{
    public GameObject Prefab;
    [Range(1, 20)] public int SpawnRate = 5;
    [Range(1, 10)] public int MaxSpawnAmount = 10;
    public bool RandomizeStats;
}