using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;
using System.Linq;

public class Bot : Character
{
    public Transform Target;
    public bool IsPlayer;

    private bool m_HasTarget;

    private Vector3 m_Waypoint;
    private Vector3 m_LastWaypoint;

    #region Init

    public void SetUpBot()
    {
        m_HasTarget = false;
        transform.localScale = Vector3.one;
        EatenBotsCount = 0;
        Target = null;
    }

    private void Awake()
    {
        SetUpBot();
        //SetSize(RandomSize());
    }

    #endregion

    #region UnityLoop

    private void Update()
    {
        Move();
        FindNearTargets();
    }

    #endregion

    #region Attacking

    private void FindNearTargets()
    {
        if (!IsPlayer || GameManager.GameState != eGameState.Playing  || BotGenerator.Instance.Bots.Count <= 0) return;

        Bot bot = this.GetClosestEnemy(BotGenerator.Instance.Bots, out float distance);
        if(Edible(bot, distance, GameConfig.Instance.BotDistanceToEat)
            && Random.value <= GameConfig.Instance.EatProbability)
        {
            Eat(bot);
        }

        float playerDis = Vector3.Distance(transform.position.SetY(0), PlayerController.Instance.transform.position);
        if (Size > PlayerController.Instance.Size && playerDis <= GameConfig.Instance.BotDistanceToEat
            && Random.value <= GameConfig.Instance.ChaseProbability)
        {
            HUDManager.Instance.SetLossReason("You were eaten!");
            GameManager.Instance.Lose();
        }

        if(m_ReachedLast)
        {
            HUDManager.Instance.SetLossReason("Someone reached king already!");
            GameManager.Instance.Lose();
        }
    }

    #endregion

    #region Movement

    private void Move()
    {
        if (Target) m_Waypoint = Target.position;

        if (Vector3.Distance(transform.position, PlayerController.Instance.transform.position) >= GameConfig.Instance.BotMaxDistance)
        {
            transform.position = BotGenerator.Instance.RandomPosition(GameConfig.Instance.BotSpawnDistance);
            m_HasTarget = false;
        }

        if (!m_HasTarget)
            m_HasTarget = CanFindTarget();
        else
        {
            RotateBot();
            m_Controller.Move(transform.forward * Time.deltaTime * GameConfig.Instance.BotMovementSpeed);
        }

        if (Vector3.Distance(transform.position, m_Waypoint) < .5f)
            m_HasTarget = false;
    }

    private bool CanFindTarget()
    {
        m_Waypoint = BotGenerator.Instance.GetWaypoint();
        if (m_Waypoint == m_LastWaypoint)
            return false;
        else
        {
            m_LastWaypoint = m_Waypoint;
            return true;
        }
    }

    private void RotateBot()
    {
        float speed = GameConfig.Instance.BotRotationSpeed * Random.Range(1, 3);
        Vector3 lookAt = m_Waypoint - transform.position;
        transform.rotation = Quaternion.Slerp
            (
            transform.rotation,
            Quaternion.LookRotation(lookAt),
            speed * Time.deltaTime
            );
    }

    #endregion
}
