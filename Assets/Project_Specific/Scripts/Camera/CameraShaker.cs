using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;
using DG.Tweening;

public class CameraShaker : Singleton<CameraShaker>
{
    #region Init
   
    private void OnEnable()
    {
        GameManager.OnLevelLoaded += OnLevelLoaded;
    }

    private void OnDisable()
    {
        GameManager.OnLevelLoaded -= OnLevelLoaded;
    }

    #endregion

    #region Callbacks

    private void OnLevelLoaded()
    {
        transform.localPosition = Vector3.zero;
    }

    #endregion

    public void Shake()
    {
        transform.DOShakePosition
            (
            GameConfig.Instance.CameraShakeDuration,
            GameConfig.Instance.CameraShakeStrength,
            GameConfig.Instance.CameraShakeVibrato,
            GameConfig.Instance.CameraShakeRandomness,
            GameConfig.Instance.CameraShakeSnapping,
            GameConfig.Instance.CameraShakeFadeOut
            );
    }
}
