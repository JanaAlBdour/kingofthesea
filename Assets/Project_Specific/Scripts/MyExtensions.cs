using System.Collections.Generic;
using System.Linq;
using SakuraGamesSDK;
using UnityEngine;
using UnityEngine.AI;

namespace SakuraGamesSDK
{
    public static class MyExtensions
    {
        public static void Shuffle<T>(this List<T> ts)
        {
            var count = ts.Count;
            var last = count - 1;
            for (var i = 0; i < last; ++i)
            {
                var r = UnityEngine.Random.Range(i, count);
                var tmp = ts[i];
                ts[i] = ts[r];
                ts[r] = tmp;
            }
        }

        public static bool IsInsideCameraView(this Vector3 i_Position)
        {
            Camera main = CameraManager.Instance.MainCamera;

            Vector3 position = main.WorldToViewportPoint(i_Position);

            bool isXInside = position.x >= 0 && position.x <= 1;
            bool isYInside = position.y >= 0 && position.y <= 1;

            bool isZInside = position.z > 0;

            return isXInside && isYInside && isZInside;
        }

        public static Vector3[] GetLinePositions(this LineRenderer i_Line, Vector3 i_AddedValue)
        {
            Vector3[] positions = new Vector3[i_Line.positionCount];
            i_Line.GetPositions(positions);

            if(i_AddedValue != Vector3.zero)
            {
                for (int i = 0; i < positions.Length; i++)
                    positions[i] += i_AddedValue;
            }

            return positions;
        }

        public static Vector3[] GetLinePositions(this LineRenderer i_Line)
        {
            Vector3[] positions = new Vector3[i_Line.positionCount];
            i_Line.GetPositions(positions);

            return positions;
        }

        public static T[] Shuffle<T>(this T[] i_Array)
        {
            var count = i_Array.Length;
            var last = count - 1;

            T[] array = i_Array;

            for (var i = 0; i < last; ++i)
            {
                var r = Random.Range(i, count);
                var tmp = array[i];
                array[i] = array[r];
                array[r] = tmp;
            }

            return array;
        }

        public static Vector3 LerpOverDistance(this Vector3[] i_Array, float t)
        {
            float totalDistance = 0;

            int arrayLength = i_Array.Length;
            for (int i = 1; i < arrayLength; i++)
                totalDistance += Vector3.Distance(i_Array[i - 1], i_Array[i]);

            float tDistance = t * totalDistance;

            float currentDistancePassed = 0;
            Vector3 pointAtT = i_Array[0];
            Vector3 closestPointTarget = pointAtT;

            for (int i = 0; i < arrayLength; i++)
            {
                Vector3 currentPointIndex = i_Array[i];
                Vector3 oldPointT = pointAtT;
                float distanceToPass = Vector3.Distance(oldPointT, currentPointIndex);
                pointAtT = currentPointIndex;

                if (currentDistancePassed + distanceToPass >= tDistance)
                {
                    distanceToPass = 0;
                    Vector3 closestPointToT = oldPointT;
                    float closestSteppedDistance = 1000;

                    for (int j = 0; j < 10; j++)
                    {
                        Vector3 steppedPoint = Vector3.Lerp(oldPointT, pointAtT, j / 10);
                        distanceToPass += Vector3.Distance(closestPointToT, steppedPoint);
                        closestPointToT = steppedPoint;
                        currentDistancePassed += distanceToPass;
                        float steppedPassedDistance = currentDistancePassed;

                        float steppedDistance = Mathf.Abs(steppedPassedDistance - tDistance);
                        if (steppedDistance < closestSteppedDistance)
                        {
                            closestSteppedDistance = steppedDistance;
                            closestPointTarget = closestPointToT;
                        }
                    }

                    break;
                }
                else
                    currentDistancePassed += distanceToPass;
            }

            return closestPointTarget;
        }

        public static bool IsNearAPoint(this Vector3 i_Point, Vector3[] i_Positions, float i_Distance)
        {   
            Vector3[] positions = i_Positions.OrderBy
                (x => Vector3.Distance(x, i_Point)).ToArray();

            for (int i = 0; i < positions.Length; i++)
            {
                float currentDistance = Vector3.Distance
                    (i_Positions[i], i_Point);

                if (currentDistance <= i_Distance)
                    return true;
            }

            return false;
        }

        public static Bot GetClosestEnemy(this List<Bot> i_Enemies, out float i_Distance)
        {
            Bot closest = i_Enemies[0];
            i_Distance = Vector3.Distance(PlayerController.Instance
                .transform.position.SetY(0), i_Enemies[0].transform.position.SetY(0));

            for (int i = 1; i < i_Enemies.Count; i++)
            {
                float distance = Vector3.Distance(PlayerController.Instance
                    .transform.position.SetY(0), i_Enemies[i].transform.position.SetY(0));
                if (distance < i_Distance)
                {
                    closest = i_Enemies[i];
                    i_Distance = distance;
                }
            }

            return closest;
        }

        public static Bot GetClosestEnemy(this Bot i_Reference, List<Bot> i_Enemies, out float i_Distance)
        {
            Bot[] bots = i_Enemies.Where(x => x != i_Reference).ToArray();

            Bot closest = bots[0];
            i_Distance = Vector3.Distance(i_Reference.transform.position.SetY(0),
                bots[0].transform.position.SetY(0));

            for (int i = 1; i < bots.Length; i++)
            {
                float distance = Vector3.Distance(i_Reference.transform.position.SetY(0), 
                    bots[i].transform.position.SetY(0));
                if (distance < i_Distance)
                {
                    closest = i_Enemies[i];
                    i_Distance = distance;
                }
            }

            return closest;
        }

        public static Vector3 SetY(this Vector3 i_RefVector, float i_NewY)
        {
            return new Vector3(i_RefVector.x, i_NewY, i_RefVector.z);
        }

        public static Vector3 SetX(this Vector3 i_RefVector, float i_NewX)
        {
            return new Vector3(i_NewX, i_RefVector.y, i_RefVector.z);
        }

        public static Vector3 SetZ(this Vector3 i_RefVector, float i_NewZ)
        {
            return new Vector3(i_RefVector.x, i_RefVector.y, i_NewZ);
        }

        public static Vector3 ScreenPosition(this Vector3 i_Input)
        {
            Camera cam = CameraFollow.Instance.MainCamera;
            return cam.WorldToScreenPoint(i_Input);
        }

        public static Vector3[] GetCirclePositions(this Vector3 i_StartPosition, int i_SegmentCount, float i_Angle, float i_Radius, 
            float i_ZDistance, float i_CircleAngle, float i_Multiplier = 1)
        {
            List<Vector3> positions = new List<Vector3>();

            float x, z;
            for (int i = 0; i < i_SegmentCount; i++)
            {
                x = Mathf.Sin(Mathf.Deg2Rad * i_Angle) * i_Radius;
                z = Mathf.Cos(Mathf.Deg2Rad * i_Angle) * i_Radius;

                z += i_ZDistance;

                Vector3 position = i_StartPosition +
                    new Vector3(x, 0, z);

                positions.Add(position);

                i_Angle += (i_CircleAngle * i_Multiplier) / i_SegmentCount;
            }

            return positions.ToArray();
        }
    }
}