using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;

//TODO: Sort out into variable editors (PlayerVars, GameplayVars, CameraVars)

//namespace SakuraGamesSDK 
//{
[CreateAssetMenu(fileName = "GameConfig", menuName = "Custom/GameConfig")]
public class GameConfig : SingletonScriptableObject<GameConfig>
{
    public float MissionCompletedIdle = 2;
    public int CoinsAdded = 50;
    public float MaxScale = 1.5f;

    [Header("Player Settings")]
    public int CountToSizeUp = 10;
    public float MovementThreshold = 0.1f;
    public float MovementSpeed = 10;
    public float RotationSpeed = 10;
    public float DistanceToEat = 0.5f;
    public float BotDistanceToEat = 0.05f;
    public float DistanceToEatPlayer = 0.25f;

    [Header("Bot Settings")]
    [Range(0, 1)]public float ChaseProbability = 0.5f;
    [Range(0, 1)]public float AIPercentage = 0.5f;
    [Range(0, 1)]public float EatProbability = 0.7f;
    public float SpawnDelay = 2;
    public float MaxBots = 30;
    public float BotSpawnDistance = 10;
    public float BotMaxDistance = 20;
    public float BotMovementSpeed = 5;
    public float BotRotationSpeed = 5;
    public float SizeMin = 1;
    public float SizeMax = 2;
    public float SizeLimiter = 30;

    [Header("Camera Shake Settings")]
    public float CameraShakeDuration = 0.2f;
    public float CameraShakeStrength = 1;
    public int CameraShakeVibrato = 10;
    public float CameraShakeRandomness = 90;
    public bool CameraShakeSnapping = false;
    public bool CameraShakeFadeOut = true;

    [Header("Camera Follow And Zoom Settings")]
    public float SmoothTime = .5f;
    public float MinimumZoom = 50f;
    public float MaximumZoom = 20f;
    public float ZoomLimiter = 50f;
    public Vector3 CameraZoomOffset = new Vector3(0, 10, 0);
    public Vector3 CameraFollowOffset = new Vector3(0, 5.5f, -2.4f);
}
//}
