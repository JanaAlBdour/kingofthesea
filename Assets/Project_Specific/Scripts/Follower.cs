using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;

public class Follower : MonoBehaviour
{
    [SerializeField] private float m_SmoothTime = 0.5f;
    
    [SerializeField] private Transform m_Target;
    private Vector3 m_Offset;
    
    private Vector3 m_Velocity;

    void Start()
    {
        m_Offset = m_Target.position - transform.position;
    }

    void LateUpdate()
    {
        Vector3 newPosition = m_Target.position + m_Offset;

        transform.position = Vector3.SmoothDamp
            (
            transform.position,
            newPosition,
            ref m_Velocity,
            m_SmoothTime
            );
    }
}
