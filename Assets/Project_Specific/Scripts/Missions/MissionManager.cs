using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;
using TMPro;
using DG.Tweening;

public class MissionManager : Singleton<MissionManager>
{
    public Mission[] Missions;
    public Mission CurrentMission;

    [SerializeField] public RectTransform CoinIcon;
    [SerializeField] private TextMeshProUGUI m_MissionText;
    [SerializeField] private TextMeshProUGUI m_RewardText;
    [SerializeField] private RectTransform m_MissionImage;

    #region Init

    private void OnEnable()
    {
        GameManager.OnLevelLoaded += OnLevelLoaded;   
    }

    private void OnDisable()
    {
        GameManager.OnLevelLoaded -= OnLevelLoaded;
        if (CurrentMission) CurrentMission.OnComplete -= OnCompleteCurrent;
    }

    #endregion

    #region Callbacks

    private void OnLevelLoaded()
    {
        SetMission();
    }

    private void OnCompleteCurrent()
    {
        CurrentMission.OnComplete -= OnCompleteCurrent;

        m_MissionText.text = "Mission Completed\n" + CurrentMission.Description;
        m_RewardText.text = CurrentMission.Reward.ToString();

        m_MissionImage.DOAnchorPosY(0, 0.5f).OnComplete
            (() => 
            {
                DOVirtual.DelayedCall(GameConfig.Instance.MissionCompletedIdle, () =>
                m_MissionImage.DOAnchorPosY(-m_MissionImage.rect.height, 0.5f), false);
            });

        StorageManager.Instance.Mission++;
        SetMission();
    }

    private void SetMission()
    {
        if (StorageManager.Instance.Mission >= Missions.Length) return;

        CurrentMission = Missions[StorageManager.Instance.Mission];
        CurrentMission.OnComplete += OnCompleteCurrent;
    }

    #endregion

    #region Unity Loop

    private void Update()
    {
        if (CurrentMission)
            CurrentMission.Check();
    }

    #endregion
}
