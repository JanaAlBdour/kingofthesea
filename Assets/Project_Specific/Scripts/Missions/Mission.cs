using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;

public class Mission : ScriptableObject
{
    public string Description;
    public int Reward = 10;
    public event System.Action OnComplete = delegate { };

    public void Complete()
    {
        OnComplete.Invoke();
        StorageManager.Instance.Coins += Reward;
        StorageManager.Instance.GainedCoins(Reward, 
            MissionManager.Instance.CoinIcon.transform.position);
    }

    public virtual void Check()
    {

    }
}
