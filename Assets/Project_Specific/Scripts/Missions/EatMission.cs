using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Eat Mission", menuName = "Custom/Missions/EatMission")]
public class EatMission : Mission
{
    public int EatCount = 5;

    public override void Check()
    {
        base.Check();
        if (PlayerController.Instance.TotalEatenFish >= EatCount)
            Complete();
    }
}
