using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;

[CreateAssetMenu(fileName = "Reach Level Mission", menuName = "Custom/Missions/ReachLevelMission")]
public class ReachLevelMission : Mission
{
    public int Level = 5;

    public override void Check()
    {
        base.Check();
        if (StorageManager.Instance.Level == Level)
            Complete();
    }
}
