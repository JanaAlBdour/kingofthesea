using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;

namespace SakuraGamesSDK
{
    public class HapticsManager : Singleton<HapticsManager>
    {
        public bool IsVibrating
        {
            get { return StorageManager.Instance.Vibrations == 1; }
            set 
            {
                if (value == true)
                    StorageManager.Instance.Vibrations = 1;
                else
                    StorageManager.Instance.Vibrations = 0;
            }
        }

        public void Haptic(HapticTypes i_Type)
        {
            if (!IsVibrating || !MMVibrationManager.HapticsSupported()) return;

            MMVibrationManager.Haptic(i_Type);
        }
    }
}