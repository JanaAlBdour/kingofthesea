using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;

public class HapticButton : MonoBehaviour
{
    [SerializeField] private GameObject m_HapticOn;
    [SerializeField] private GameObject m_HapticOff;

    public void Switch()
    {
        HapticsManager.Instance.IsVibrating = !HapticsManager.Instance.IsVibrating;
        SetValues();
    }

    private void Start()
    {
        SetValues();
    }

    private void SetValues()
    {
        bool haptics = HapticsManager.Instance.IsVibrating;
        m_HapticOn.SetActive(haptics);
        m_HapticOff.SetActive(!haptics);
    }
}
