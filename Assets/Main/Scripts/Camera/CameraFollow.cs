using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;
//using DG.Tweening;

public class CameraFollow : Singleton<CameraFollow>
{
    public bool IsZooming;

    [SerializeField] private Transform[] m_Targets;
    [SerializeField] public Camera MainCamera;

    private Vector3 m_Velocity;
    private float m_StartFOV;
    private Vector3 m_StartPosition;

    #region Init

   // public override 
    public override void Awake()
    {
        base.Awake();
        m_StartFOV = MainCamera.fieldOfView;
        m_StartPosition = MainCamera.transform.position;
    }

    private void OnEnable()
    {
        GameManager.OnLevelLoaded += ResetVariables;
    }

    private void OnDisable()
    {
        GameManager.OnLevelLoaded -= ResetVariables;
    }

    #endregion

    #region Callbacks

    public void ResetVariables()
    {
        IsZooming = false;

        MainCamera.fieldOfView = m_StartFOV;
        MainCamera.transform.position = m_StartPosition;
    }

    #endregion

    #region Unity Loop

    private void LateUpdate()
    {
        if (m_Targets.Length <= 0) return;

        Move();
        Zoom();
    }

    void Zoom()
    {
        if (!IsZooming) return;

        float newZoom = Mathf.Lerp
            (
            GameConfig.Instance.MaximumZoom,
            GameConfig.Instance.MinimumZoom, 
            GetGreatestDistance() / GameConfig.Instance.ZoomLimiter
            );

        MainCamera.fieldOfView = Mathf.Lerp
            (
            MainCamera.fieldOfView, 
            newZoom, 
            Time.deltaTime
            );
    }

    void Move()
    {
        Vector3 centerPoint = GetCenterPoint();

        Vector3 offset = IsZooming ? GameConfig.Instance.CameraZoomOffset : GameConfig.Instance.CameraFollowOffset;
        Vector3 newPosition = centerPoint + offset;

        transform.position = Vector3.SmoothDamp
            (
            transform.position, 
            newPosition, 
            ref m_Velocity, 
            GameConfig.Instance.SmoothTime
            );

        transform.LookAt(centerPoint);
    }

    float GetGreatestDistance()
    {
        Bounds bounds = new Bounds(m_Targets[0].transform.position, Vector3.zero);
        for (int i = 0; i < m_Targets.Length; i++)
            bounds.Encapsulate(m_Targets[i].position);

        return Mathf.Max(bounds.size.x, bounds.size.z);
    }

    public Vector3 GetCenterPoint()
    {
        if (m_Targets.Length == 1)
            return m_Targets[0].position;

        Bounds bounds = new Bounds(m_Targets[0].position, Vector3.zero);
        for (int i = 0; i < m_Targets.Length; i++)
            bounds.Encapsulate(m_Targets[i].position);
        
        return bounds.center;
    }

    #endregion
}
