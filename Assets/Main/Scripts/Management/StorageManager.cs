using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SakuraGamesSDK
{
    public class StorageManager : Singleton<StorageManager>
    {
        public static event System.Action<int, Vector3> OnGainCoins = delegate { };

        public void GainedCoins(int i_GainedAmount, Vector3 i_Position)
        {
            OnGainCoins.Invoke(i_GainedAmount, i_Position);
        }

        public int Level
        {
            get { return PlayerPrefs.GetInt("Level", 1); }
            set { PlayerPrefs.SetInt("Level", value); }
        }

        public int Coins
        {
            get { return PlayerPrefs.GetInt("Coin"); }
            set { PlayerPrefs.SetInt("Coin", value); }
        }

        public int Vibrations
        {
            get { return PlayerPrefs.GetInt("Vibrations", 1); }
            set { PlayerPrefs.SetInt("Vibrations", value); }
        }

        public int Mission
        {
            get { return PlayerPrefs.GetInt("Mission", 0); }
            set { PlayerPrefs.SetInt("Mission", value); }
        }

        private void Update()
        {
            if (Application.isEditor && Input.GetKeyDown(KeyCode.R))
                PlayerPrefs.DeleteAll();
        }
    }
}