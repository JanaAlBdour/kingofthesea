using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MoreMountains.NiceVibrations;
using Facebook.Unity;

namespace SakuraGamesSDK 
{
    public class GameManager : Singleton<GameManager>
    {
        public static eGameState GameState;

        public static event System.Action OnLevelLoaded = delegate { };
        public static event System.Action OnLevelStarted = delegate { };
        public static event System.Action OnLevelCompleted = delegate { };
        public static event System.Action OnLevelFailed = delegate { };
        public static event System.Action OnLevelReset = delegate { };

        void Start()
        {
            FB.Init();
            LoadLevel();
        }

        public void LoadLevel()
        {
            OnLevelLoaded.Invoke();
            GameState = eGameState.Idle;
        }

        public void Complete()
        {
            HapticsManager.Instance.Haptic(HapticTypes.Success);

            OnLevelCompleted.Invoke();
            StorageManager.Instance.Level++;
            GameState = eGameState.Completed;

            StorageManager.Instance.Coins += GameConfig.Instance.CoinsAdded;

        }

        public void Lose()
        {
            OnLevelFailed.Invoke();
            GameState = eGameState.Lost;

            HapticsManager.Instance.Haptic(HapticTypes.Failure);
        }

        public void Next()
        {
            LoadLevel();
        }

        public void StartLevel()
        {
            OnLevelStarted.Invoke();
            GameState = eGameState.Playing;
        }
    }
}
