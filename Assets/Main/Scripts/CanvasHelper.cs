using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Canvas))]
public class CanvasHelper : MonoBehaviour
{
    private static List<CanvasHelper> m_Helpers = new List<CanvasHelper>();

    public static UnityEvent OnResolutionOrOrientationChanged = new UnityEvent();

    private static bool m_ScreenChangeVarsInitialized = false;
    private static ScreenOrientation m_LastOrientation = ScreenOrientation.Landscape;
    private static Vector2 m_LastResolution = Vector2.zero;
    private static Rect m_LastSafeArea = Rect.zero;

    [SerializeField] private Canvas m_Canvas;
    [SerializeField] private RectTransform m_RectTransform;
    [SerializeField] private RectTransform m_SafeAreaTransform;

    void Awake()
    {
        if (!m_Helpers.Contains(this))
            m_Helpers.Add(this);

        if (!m_ScreenChangeVarsInitialized)
        {
            m_LastOrientation = Screen.orientation;
            m_LastResolution.x = Screen.width;
            m_LastResolution.y = Screen.height;
            m_LastSafeArea = Screen.safeArea;

            m_ScreenChangeVarsInitialized = true;
        }

        ApplySafeArea();
    }

    void Update()
    {
        if (m_Helpers[0] != this)
            return;

        if (Application.isMobilePlatform && Screen.orientation != m_LastOrientation)
            OrientationChanged();

        if (Screen.safeArea != m_LastSafeArea)
            SafeAreaChanged();

        if (Screen.width != m_LastResolution.x || Screen.height != m_LastResolution.y)
            ResolutionChanged();
    }

    void ApplySafeArea()
    {
        if (m_SafeAreaTransform == null)
            return;

        var safeArea = Screen.safeArea;

        var anchorMin = safeArea.position;
        var anchorMax = safeArea.position + safeArea.size;
        anchorMin.x /= m_Canvas.pixelRect.width;
        anchorMin.y /= m_Canvas.pixelRect.height;
        anchorMax.x /= m_Canvas.pixelRect.width;
        anchorMax.y /= m_Canvas.pixelRect.height;

        m_SafeAreaTransform.anchorMin = anchorMin;
        m_SafeAreaTransform.anchorMax = anchorMax;
    }

    void OnDestroy()
    {
        if (m_Helpers != null && m_Helpers.Contains(this))
            m_Helpers.Remove(this);
    }

    private static void OrientationChanged()
    {
        m_LastOrientation = Screen.orientation;
        m_LastResolution.x = Screen.width;
        m_LastResolution.y = Screen.height;

        OnResolutionOrOrientationChanged.Invoke();
    }

    private static void ResolutionChanged()
    {
        m_LastResolution.x = Screen.width;
        m_LastResolution.y = Screen.height;

        OnResolutionOrOrientationChanged.Invoke();
    }

    private static void SafeAreaChanged()
    {
        m_LastSafeArea = Screen.safeArea;

        for (int i = 0; i < m_Helpers.Count; i++)
        {
            m_Helpers[i].ApplySafeArea();
        }
    }
}
