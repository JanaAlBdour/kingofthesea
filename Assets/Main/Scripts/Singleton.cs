using UnityEngine;
using System.Collections;

namespace SakuraGamesSDK 
{ 
    public class Singleton<T> : MonoBehaviour where T : Component
    {
        public bool DestroyOnLoad;
        private static T m_Instance;
        public static T Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = FindObjectOfType<T>();
                    if (m_Instance == null)
                    {
                        GameObject obj = new GameObject();
                        obj.name = typeof(T).Name;
                        m_Instance = obj.AddComponent<T>();
                    }
                }
                return m_Instance;
            }
        }

        public virtual void Awake()
        {
            if (m_Instance == null)
            {
                m_Instance = this as T;
                if (!DestroyOnLoad)
                    DontDestroyOnLoad(this.gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
}
    }