using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SakuraGamesSDK;
using TMPro;

namespace SakuraGamesSDK
{
    public class HUDManager : Singleton<HUDManager>
    {
        [SerializeField] private TextMeshProUGUI m_LossReason;
        [SerializeField] private TextMeshProUGUI m_LevelText;
        [SerializeField] private UIAnimator m_StartMenu;
        [SerializeField] private UIAnimator m_WinMenu;
        [SerializeField] private UIAnimator m_LoseMenu;
        [SerializeField] private UIAnimator m_InGameMenu;

        #region Init

        private void OnEnable()
        {
            GameManager.OnLevelLoaded += OnLevelLoaded;
            GameManager.OnLevelStarted += OnLevelStarted;
            GameManager.OnLevelFailed += OnLevelFailed;
            GameManager.OnLevelCompleted += OnLevelCompleted;
        }

        private void OnDisable()
        {
            GameManager.OnLevelLoaded -= OnLevelLoaded;
            GameManager.OnLevelStarted -= OnLevelStarted;
            GameManager.OnLevelFailed -= OnLevelFailed;
            GameManager.OnLevelCompleted -= OnLevelCompleted;
        }

        #endregion

        #region Callbacks

        private void OnLevelLoaded()
        {
            m_StartMenu.gameObject.SetActive(true);
            m_LoseMenu.Hide();
            m_WinMenu.Hide();
            m_InGameMenu.Hide();
        }

        private void OnLevelStarted()
        {
            m_LevelText.text = "LEVEL " + StorageManager.Instance.Level;

            m_InGameMenu.gameObject.SetActive(true);
            m_StartMenu.Hide();
            m_LoseMenu.Hide();
            m_WinMenu.Hide();
        }

        private void OnLevelFailed()
        {
            m_LoseMenu.gameObject.SetActive(true);
            m_StartMenu.Hide();
            m_WinMenu.Hide();
            m_InGameMenu.Hide();
        }

        private void OnLevelCompleted()
        {
            m_WinMenu.gameObject.SetActive(true);
            m_StartMenu.Hide();
            m_LoseMenu.Hide();
            m_InGameMenu.Hide();
        }

        #endregion

        public void SetLossReason(string i_Reason)
        {
            m_LossReason.text = i_Reason; 
        }
    }

}